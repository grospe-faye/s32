const Course = require('../models/Course');
const auth = require('../auth');

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({	
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// Retrieve all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	/*	let getCourse = {
			name: result.name,
			price: result.price
		}
		return getCourse*///returns name and price only
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// ACTIVITY 11-04-2021
// Archive a course
module.exports.archiveCourse = (reqParams, reqBody) =>{
	let archiveCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}